# README #

An example solution that demonstrates various mocking/faking NuGet packages that can be used along side NUnit for writing more robust unit tests.

Example packages: NSubstitute, RhinoMocks, Moq, FakeItEasy

Includes a simple model of getting a user by ID with a domain module and repository.  The subject user test is the user domain module.


### Who do I talk to? ###

Owner - kiwkmark