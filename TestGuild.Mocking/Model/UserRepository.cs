﻿using System;
using TestGuild.Mocking.Interfaces;

namespace TestGuild.Mocking.Model
{
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IUser GetUserById(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
