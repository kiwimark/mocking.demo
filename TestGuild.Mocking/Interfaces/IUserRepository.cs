﻿namespace TestGuild.Mocking.Interfaces
{
    public interface IUserRepository
    {
        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IUser GetUserById(string userId);
    }
}
