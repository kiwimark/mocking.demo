﻿using FakeItEasy;
using NUnit.Framework;
using TestGuild.Mocking.Interfaces;
using TestGuild.Mocking.Model;

// ReSharper disable InconsistentNaming
namespace TestGuild.Mocking.Tests
{
    [TestFixture]
    public class Test_FakeItEasy_GetUserById
    {
        // Example of mocking a repository using FakeItEasy

        [Test]
        public void FakeItEasy_GetUser_ValidID_CorrectUserReturned()
        {
            const string USER_ID = "123456789";
            const string FIRST_NAME = "John";
            const string LAST_NAME = "Doe";
            const string USER_NAME = "JDoe";

            // ------------ Arrange ------------

            // Create a fake user repository
            IUserRepository userRepositoryFake = A.Fake<IUserRepository>();
            
            // Mock method GetUserById() to return what we expect from the user repository
            A.CallTo(() => userRepositoryFake.GetUserById(USER_ID)).Returns(new User
                                {
                                    FirstName = FIRST_NAME,
                                    LastName = LAST_NAME,
                                    UserName = USER_NAME,
                                });


            // Inject the user repository into the user domain module constructor
            UserDomainModule sut = new UserDomainModule(userRepositoryFake);

            // ------------ Act ------------
            IUser user = sut.GetUser(USER_ID);

            // ------------ Assert ------------ 
            Assert.IsNotNull(user, "The response from UserDomainModule.GetUser is null");
            Assert.AreEqual(user.FirstName, FIRST_NAME, "Incorrect first name value");
            Assert.AreEqual(user.LastName, LAST_NAME, "Incorrect last name value");
            Assert.AreEqual(user.UserName, USER_NAME, "Incorrect user name value");
        }
    }
}
