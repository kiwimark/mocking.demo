﻿namespace TestGuild.Mocking.Interfaces
{
    public interface IUserDomainModule
    {
        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        IUser GetUser(string userId);
    }
}
