﻿using System;
using TestGuild.Mocking.Interfaces;

namespace TestGuild.Mocking.Model
{
    public class UserDomainModule : IUserDomainModule
    {
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDomainModule"/> class.
        /// </summary>
        /// <param name="userRepository">The user repository.</param>
        public UserDomainModule(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">userId;A user ID must be provided.</exception>
        public IUser GetUser(string userId)
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ArgumentNullException("userId", "A user ID must be provided.");
            }

            IUser user = _userRepository.GetUserById(userId);

            return user;
        }
    }
}
